/*Query Operators (comparison)
	$gt(greater than)
	$lt(less than)
	
*/

/*$gt (greater than) - $gte (greater than or equal to)

	Syntax:

	db.collectionName.find({field: {$gt: value}});

	db.collectionName.find({field: {$gte: value}});
*/
db.users.find({"age": {$gt: 80}});
db.users.find({"age": {$gt: 70}});

/*$lt (less than) - $lte (less than or equal to)

	Syntax:

	db.collectionName.find({field: {$lt: value}});

	db.collectionName.find({field: {$lte: value}});

*/

/*$ne (not equal)
	
	Syntax:

	db.collectionName.find({field: {$ne: value}});
*/
db.users.find({"age": {$ne: 82}});

/*$in (include)
	
	Syntax:
	
	db.collectionName.find({field: {$in: [value, ...]}});
*/
db.users.find({"lastName": {$in: ["Hawking", "Gates"]}});

// (Logical) Query Operators

/*$or
	
	Syntax:
	
	db.collectionName.find({$or:[{fieldA: valueA}, {fieldB: valueB}] });
*/
db.users.find({
	$or: [{"firstName": "Neil"}, {"age":25}] 
});
db.users.find({
	$or: [{"firstName": "Neil"}, {"age":{$gt: 30}}] 
});


/*$and
	Syntax:

	db.collectionName.find({$and:[{fieldA: valueA}, {fieldB: valueB}] });
*/
db.users.find({ 
	$and: [{"age":{$ne:76}}, {"age":{$ne:76}}] 
});
db.users.find({ 
	$and: [{"age":{$ne:82}}, {"age":{$ne:76}}] 
});

// Field Projections


/*Inclusion
	-fields are included to resulting documents.

	Syntax:
	db.collectionName.find({field:value}, {field: 1, ...});
*/
db.users.find(
		{"firstName": {$ne: "Jane"}},
		{"firstName": 1}
	);



/*Exclusion
	-fields are excluded to the resulting documents.

	Syntax:
	db.collectionName.find({field:value}, {field: 0});
*/
db.users.find(
		{"firstName": {$ne: "John"}},
		{"_id": 0, "contact": 0, "department": 0}		
);

/*Inclusion/exclusion of nested document fields

	Syntax (inclusion):
	
	db.collectionName.find({field:value}, {field.nestedfield: 1, ...});
	
	Syntax (exclusion):
	
	db.collectionName.find({field:value}, {field.nestedfield: 0, ...});


*/
db.users.find(
		{"age": {$gt: 75}},
		{"firstName": 1, "lastName": 1, "contact.phone": 1}		
);

/*(Evaluation) Query Operator*/

/*$regex --> patterns (case-sensitive)
	
	Syntax:
	db.collectionName.find({field: $regex:"pattern", $options: $optionValue});
*/
db.users.find({
	"firstName": {$regex:"n"} 
});

// case-insensitive
db.users.find({
	"firstName": {$regex:"n", $options: "$i"}
});
